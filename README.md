# Augmentory 

## What is Augmentory

It is a way to automate the generation of code based on a set of parameters generated from sketch file.

## How to use: 

1. Apply changes to sketch file
2. Push changes using sketch to git plugin or manually to the repository
3. gitlab will automatically update the HTML files
4. you will be notified on email and slack when the build succeeds
5. enjoy your updated page :)