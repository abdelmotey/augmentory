const ns = require('node-sketch');
const handlebars = require('handlebars');
const fs = require('fs');
const flatMap = require('lodash/flatMap')

const SKETCH_FILE_NAME = 'demo.sketch';
ns.read('sketch/' + SKETCH_FILE_NAME).then(sketch => {
    // const foreignSymbols = flatMap(sketch.foreignSymbols, s => s.layers);
    var hasImages = false;

    COMPONENT_PARAMETERS_MAP = {
        "Modules/03 Tiles/00 Main-Dashboard/Desktop": {
            "Title": "title",
            "Subtitle": "subtitle",
            "Icon / Text": "icon"
        },
        "Modules\/05 Notification\/General\/Red\/Desktop": {
            "Title": "title",
            "Text": "subtitle"
        }
    }

    function getSymbolData (obj) {
        let masterLayers = flatMap(obj.symbolMaster.layers, s => s.layers);
        const name = obj.name;

        // POC: only text attributes
        let overrides = obj.overrideValues.map(v => {
            let overrideName = v.overrideName.split('_')[0].split('/')[0];
            let overrideType = v.overrideName.split('_')[1];
            let base =  masterLayers.filter(l => l.do_objectID === overrideName)[0]

            let val = overrideType === 'symbolID' ? sketch.foreignSymbols.filter(l => l.symbolID === v.value)[0].name : v.value;

            return {
                name: base ? base.name: '',
                value: val
            }
        });

        return overrides.reduce((acc, obj)=> {
            COMPONENT_PARAMETERS_MAP[name][obj.name]
            acc[COMPONENT_PARAMETERS_MAP[name][obj.name]]=obj.value; return acc
        },{});
    }

    function getImageData (obj) {
        hasImages = true;
        return obj.image.file
    }

    function rgba2hex(orig) {
        var rgb = orig.replace(/\s/g, '').match(/^rgba?\((\d+\.?\d*),(\d+\.?\d*),(\d+\.?\d*),?([^,\s)]+)?/i),
          hex = rgb ?
          (Math.floor(rgb[1] * 255) | 1 << 8).toString(16).slice(1) +
          (Math.floor(rgb[2] * 255) | 1 << 8).toString(16).slice(1) +
          (Math.floor(rgb[3] * 255) | 1 << 8).toString(16).slice(1) : orig;
        return "#" + hex;
    }

    function getColor (colorObj) { 
        return rgba2hex(`rgba(${colorObj.red},${colorObj.green},${colorObj.blue},${colorObj.alpha})`)
    }

    function getStyles (obj) { 
        let style = {
            height: obj.frame.height,
            width: obj.frame.width
        };

        if(!obj.style) { return style; }

        if(obj.style.borders && obj.style.borders.length > 0) {
            let border = obj.style.borders[0];
            let color = getColor(border.color)
            style.border = `${border.thickness}px solid ${color}`
        }

        if(obj.style.fills && obj.style.fills.length > 0) {
            let fillColor = getColor(obj.style.fills[0].color);
            style.background = `${fillColor}`
        }

        return style; 
    }
    
    function mapLayer(obj) {
        let mapedObj = {
            id: obj.do_objectID,
            class: obj._class,
            isVisible: obj.isVisible,
            style: getStyles(obj),
            name: obj.name
        }
        if (obj.layers) mapedObj.components = obj.layers;
        if (obj._class === "symbolInstance") mapedObj.parameters = getSymbolData(obj);
        if (obj._class === "bitmap") mapedObj.file = getImageData(obj);
        
        return mapedObj;
    }

    function getComponentList(arr) {
        return arr.map(c => {
            if(c.layers) {
                c.layers = getComponentList(c.layers);
                return mapLayer(c)
            } else { 
                return mapLayer(c)
            }
        })
    }
            
    var lst = getComponentList(sketch.pages)
    var pageComponents = lst[0].components[0].components;

    if(hasImages) {
        const exec = require('child_process').exec;
        exec('sh ./copyAssets.sh ' + SKETCH_FILE_NAME, (error, stdout, stderr) => {
            // console.log(stdout);
            console.log(stderr);
            if (error !== null) {
                console.log(`exec error: ${error}`);
            }
        });
    }

    fs.writeFile('./metadata.json', JSON.stringify(lst, null, 4), 'utf8', function (err) {
        if (err) {throw err;}
    })

    const COMPONENTS_DICTIONARY = { 
        "Modules/03 Tiles/00 Main-Dashboard/Desktop": "tile",
        "rectangle": "rect",
        "bitmap": "image",
        "Modules\/05 Notification\/General\/Red\/Desktop": "notification"
    }

    var finalHTML = "";

    pageComponents.forEach(comp => {
        var selector = comp.class === 'symbolInstance' ? comp.name : comp.class;
        let hbs = fs.readFileSync(`./snippets/${COMPONENTS_DICTIONARY[selector]}.hbs`, 'utf-8') 

        if(!hbs) return;
        
        var template = handlebars.compile(hbs);
        var html = template(comp);
        finalHTML += html;
    });

    let page = handlebars.compile(fs.readFileSync(`./snippets/page.hbs`, 'utf-8'))
    let pageHtml = page({content: finalHTML})
    console.log(pageHtml);
 
});