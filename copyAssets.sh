echo "File Name: ${1}"
cd sketch
mkdir tmp && cp ${1} tmp
cd tmp
unzip ${1}
cp images/* ../../images/
cd ../
rm -R tmp
cd ../

